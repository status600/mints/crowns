


mv /usr/lib/python3.11/EXTERNALLY-MANAGED /usr/lib/python3.11/EXTERNALLY-MANAGED.old


#
#
#	Mongo
#
#
apt-get update
apt install gnupg curl -y
curl -fsSL https://www.mongodb.org/static/pgp/server-8.0.asc | \
   gpg -o /usr/share/keyrings/mongodb-server-8.0.gpg \
   --dearmor
   
# Ubuntu 22
echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-8.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/8.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-8.0.list

apt-get update
apt-get install -y mongodb-org


#\
#
#	NVM + Node.js
#	
#
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install 20
#
#/

#\
#
#	PNPM
#	
#
export SHELL=/bin/bash 
curl -fsSL https://get.pnpm.io/install.sh | sh -
. /root/.bashrc
#
#/



#\
#
#	build
#
deactivate

cd /habitat && rm -rf .venv
pip install uv poetry
cd /habitat && rm requirements.txt
cd /habitat && uv pip compile pyproject.toml -o requirements.txt

sleep 1

cd /habitat && uv venv
cd /habitat && . /habitat/.venv/bin/activate
cd /habitat && uv pip sync requirements.txt
#
#\

#/
apt install unzip; curl -fsSL https://bun.sh/install | bash; 

export PATH=$PATH:/root/.bun/bin
(cd /habitat/parties_frontend/sveltenetics && rm -rf node_modules)
(cd /habitat/parties_frontend/sveltenetics && pnpm install)


. /habitat/_controls/build_2.sh




