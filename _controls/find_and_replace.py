
'''
	python3 /habitat/_controls/find_and_replace.py
'''

'''
	[ ] exclude moon data
'''	

import pathlib
from os.path import dirname, join, normpath
import sys
def add_paths_to_system (paths):
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'stages_pip',
	'stages'
])

import ships.paths.directory.find_and_replace_string_v2 as find_and_replace_string_v2

this_directory = pathlib.Path (__file__).parent.resolve ()

directories = [
	"/habitat/pyproject.toml",
	"/habitat/docker-compose.yml",
	"/habitat/Venue.O.HTML",
	
	"/habitat/_controls/build_1.sh",
	"/habitat/_controls/build_2.sh",

	str (normpath (join (this_directory, "../parties"))),
	str (normpath (join (this_directory, "../venues/_controls")))
]

for directory in directories:
	proceeds = find_and_replace_string_v2.start (
		the_path = directory,

		find = 'mediphy',
		replace_with = 'prosthetic',
		
		replace_contents = "yes",
		replace_paths = "yes"
	)
	
	#print (proceeds)