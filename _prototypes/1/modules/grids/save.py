


import io
import zipfile
import pymongo
from gridfs import GridFS

# Connect to MongoDB
client = pymongo.MongoClient('mongodb://localhost:27017/')
db = client['your_database']
fs = GridFS(db, collection='folder_archives')

# Function to zip a folder and save it to MongoDB using GridFS
def zip_and_save_to_gridfs (
	folder_path, 
	metadata = None,
	GridFS_driver = None
):
    zip_buffer = io.BytesIO ()
    with zipfile.ZipFile (zip_buffer, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, dirs, files in os.walk(folder_path):
            for file in files:
                file_path = os.path.join(root, file)
                zipf.write(file_path, os.path.relpath(file_path, folder_path))

    zip_buffer.seek (0)

    fs.put (
		zip_buffer, 
		filename='folder_archive.zip', 
		metadata=metadata
	)

# Specify the folder path to zip and save to MongoDB
folder_to_zip = '/path/to/your/folder'
metadata = {'name': 'zipped_folder'}

# Zip the folder and save it to MongoDB using GridFS
zip_and_save_to_gridfs(folder_to_zip, metadata)

print("Folder zipped and saved to MongoDB using GridFS.")
