



import zipfile
import os
def unzip_folder(zip_file, extract_to):
	try:
		with zipfile.ZipFile(zip_file, 'r') as zip_ref:
			zip_ref.extractall(extract_to)
		return True
	except Exception as e:
		print(f"Error unzipping folder: {e}")
		return False


