

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_directory = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'modules'
])

	
import zipfile
import os	
import pathlib
from os.path import dirname, join, normpath
import sys
from pymongo import MongoClient
from bson.binary import Binary
from gridfs import GridFS


from grids.save import zip_and_save_to_gridfs

import io
import zipfile
import pymongo
from gridfs import GridFS

# Connect to MongoDB
client = pymongo.MongoClient('mongodb://localhost:27017/')
db = client['your_database']
fs = GridFS(db, collection='folder_archives')