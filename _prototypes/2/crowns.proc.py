



import pathlib
from os.path import dirname, join, normpath
import sys
def add_paths_to_system (paths):	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../modules/structures',
	'../../modules/structures_pip'
])

import crowns
crowns.clique ()