


import io
import zipfile
import pymongo
from gridfs import GridFS
import os

'''
	zip_and_save_to_gridfs (
		folder_path = str (normpath (join (this_directory, 'folder'))), 
		metadata = None,
		GridFS_driver = None
	)
'''
def zip_and_save_to_gridfs (
	folder_path, 
	metadata = {},
	
	GridFS_collection = None,
	GridFS_collection_files = None
):
	zip_buffer = io.BytesIO ()
	with zipfile.ZipFile (zip_buffer, 'w', zipfile.ZIP_DEFLATED) as zipf:
		for root, dirs, files in os.walk(folder_path):
			for file in files:
				file_path = os.path.join(root, file)
				zipf.write (
					file_path, 
					os.path.relpath(file_path, folder_path)
				)

	zip_buffer.seek (0)

	id = GridFS_collection.put (
		zip_buffer, 
		filename = 'folder_archive.zip', 
		metadata = metadata
	)
	
	GridFS_collection_files.update_one ({'_id': id }, {'$set': {'uploadDate': '' }})
	
	
	print ("id:", id)
	
	
	return id;


