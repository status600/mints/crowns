
******

Bravo!  You have received a Mercantilism Diploma in "prosthetic" from   
the Orbital Convergence University International Air and Water 
Embassy of the tangerine Planet (the planet that is one ellipse further from
the Sun than Earth's ellipse).

You are now officially certified to include "prosthetic" in your practice.

******

Please feel free to use this module however (business, personal, etc.)
subject to the terms of AGPL 3.0 License, or any of the other licenses
in the the pypi "prosthetic/_licenses/options" directory.

	@ BGraceful

******


# prosthetic

******

## obtain
`[xonsh] pip install prosthetic`

******

## school
`[xonsh] prosthetic school`

******

## structure
```
	/prosthetic_1
		/trends
		/treasures
			/treasure.1
			/treasure.2
		
		prosthetic_essence.py
			essence = {}
```

## on
From a directory that is after the "prosthetic_essence.py" file.   
```
[@ prosthetic_1]
prosthetic ventures on
prosthetic form
```

## off
```
[@ prosthetic_1]
prosthetic ventures off
```

## refresh
```
[@ prosthetic_1]
prosthetic ventures refresh
```

******

## Treasures
### itemize
```
[@ prosthetic_1]
prosthetic treasures itemize
```

### store
```
[@ prosthetic_1]
prosthetic treasures stow --name "garbage.1"
```


******

## Trends
### itemize
```
[@ prosthetic_1]
prosthetic trends itemize
```

### search
```
[@ prosthetic_1]
prosthetic trends search --domain "wallet.1"
```

### insert-document
```
[@ prosthetic_1]
prosthetic trends insert-document --domain "wallet.1" --names "[ 'name_1', 'name_2' ]"`
prosthetic trends insert-document --domain "solid_food.1" --topics "[ 'food' ]" --cautions "[ 'homo-sapiens ages months 6+' ]"
```

******

## Trends Saving
### save export
This saves the collections as .export files, sort of like JSON.  
	
```	
[@ prosthetic_1]
prosthetic trends saves exports-export --version 1
```

### import export-import-overwrite
This deletes the collections, and then imports the save.  

```
[@ prosthetic_1]
prosthetic trends saves exports-import-overwrite --version 1
```

### import export-import-merge
This merges the save into the current collection.  
	
```
[@ prosthetic_1]
prosthetic trends saves exports-import-overwrite --version 
```