


/*	
import {
	lease_behavior_truck,
	give_back_behavior_truck
} from '$lib/Behavior/Trucks'

*/
/*
	import { ask_for_freight } from '$lib/Behavior/Trucks'
	const freight = ask_for_freight ();
*/


/*	
import { onMount, onDestroy } from 'svelte'
import { check_behavior_truck, monitor_behavior_truck } from '$lib/Behavior/Trucks'

let RT_Prepared = "no"
let RT_Monitor;
let RT_Freight;
onMount (async () => {
	const Truck = check_behavior_truck ()
	RT_Freight = Truck.freight; 
	
	RT_Monitor = monitor_behavior_truck ((_freight) => {
		RT_Freight = _freight;
	})
	
	RT_Prepared = "yes"
});

onDestroy (() => {
	RT_Monitor.stop ()
}); 

// RT_Freight.net_path
// RT_Freight.net_name
*/


/*	
import { ask_for_freight } from '$lib/Behavior/Trucks'
const { mongo_address } = ask_for_freight ();
*/

import * as AptosSDK from "@aptos-labs/ts-sdk";
import { build_truck } from '@visiwa/trucks'
const trucks = {}

export const lease_behavior_truck = () => {
	
	/*
		localStorage.setItem ("origin_address", "http://192.168.0.98:22000")
	*/
	let origin_address = window.location.protocol + '//' + window.location.host
	if (typeof localStorage.origin_address === "string") {
		origin_address = localStorage.origin_address	
	}
	
	
	/*
	let Mongo_Address = "business"
	if (typeof localStorage.Mongo_Address === "string") {
		Mongo_Address = localStorage.Mongo_Address	
	}
	*/
	
	trucks [1] = build_truck ({
		freight: {
			origin_address
			
		}
	});
	
	let monitor = trucks [1].monitor (async ({
		original_freight,
		pro_freight, 
		//
		bracket,
		//
		property, 
		value
	}) => {
		try {
			// leaf name changed
			if (bracket === original_freight && property === "Mongo_Address") {
				console.info ("Mongo_Address changed");
			}
		}
		catch (imperfection) {
			console.error (imperfection);
		}
	});
}


export const destroy = () => {
	delete trucks [1];
}

export const check_behavior_truck = () => {
	return trucks [1];
}
export const ask_for_freight = () => {
	return trucks [1].freight;
}

export const monitor_behavior_truck = (action) => {	
	return trucks [1].monitor (({ freight }) => {
		console.info ('Behavior Truck_Monitor', { freight })
		action (freight);
	})
}







