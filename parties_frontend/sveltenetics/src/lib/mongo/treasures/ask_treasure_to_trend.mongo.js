


/*
	import { ask_treasure_to_trend } from '$lib/mongo/treasures/ask_treasure_to_trend.mongo.js'
	const { enhanced } = await ask_treasure_to_trend ({
		domain: "domain_1"
	});
*/

import { ask_for_freight } from '$lib/Behavior/Trucks'

export const ask_treasure_to_trend = async (packet) => {
	console.log ({ packet });
	
	const { origin_address } = ask_for_freight ();
	const proceeds = await fetch (origin_address + "/monetary/treasures/treasure_to_trend", {
		method: 'POST',
		body: JSON.stringify (packet)
	});
	const enhanced = await proceeds.json ()

	return {
		enhanced
	};
}

