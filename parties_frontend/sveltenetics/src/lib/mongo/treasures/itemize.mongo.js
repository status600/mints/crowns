





// https://aptos.dev/en/build/apis/fullnode-rest-api-reference#tag/general/get/

/*
	import { ask_for_list_of_treasures } from '$lib/mongo/treasures/itemize.mongo.js'
	const { enhanced } = await ask_for_list_of_treasures ()
*/

import { ask_for_freight } from '$lib/Behavior/Trucks'

export const ask_for_list_of_treasures = async () => {
	const { origin_address } = ask_for_freight ();
	const proceeds = await fetch (origin_address + "/monetary/treasures/itemize");
	const enhanced = await proceeds.json ()

	return {
		enhanced
	};
}