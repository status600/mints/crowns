







/*
	https://aptos.dev/en/build/apis/fullnode-rest-api-reference#tag/general/get/

*/

/*
	import { ask_to_delete_trend } from '$lib/mongo/treasures/ask_to_delete_trend.mongo.js'
	const { enhanced } = await ask_to_delete_trend ({
		domain: "domain"
	})
*/

import { ask_for_freight } from '$lib/Behavior/Trucks'

export const ask_to_delete_trend = async (packet) => {
	const { origin_address } = ask_for_freight ();
	const proceeds = await fetch (origin_address + "/monetary/trend/delete", {
		method: 'POST',
		body: JSON.stringify (packet)
	});
	
	const status = proceeds.ok;
	const enhanced = await proceeds.json ();

	return { 
		enhanced,
		status
	};
}