


/*
	import { ask_trend_to_treasure } from '$lib/mongo/trends/ask_count.mongo.js'
	const { enhanced } = await ask_trend_to_treasure ({
		domain: "domain_1"
	})
*/

import { ask_for_freight } from '$lib/Behavior/Trucks'

export const ask_trend_to_treasure = async (packet) => {
	const { origin_address } = ask_for_freight ();
	const proceeds = await fetch (origin_address + "/monetary/trends/trend_to_treasure", {
		method: 'POST',
		body: JSON.stringify (packet)
	});
	const enhanced = await proceeds.json ()

	return {
		enhanced
	};
}