
/*
	filter by: 
		treasure.name
*/
export const filter_treasures_by_domain = ({ domain_filter, treasures }) => {
	if (domain_filter === "") {
		return treasures
	}
	
	const fresh_treasures = treasures.filter (treasure => {
		try {
			if (treasure.domain.indexOf (domain_filter) >= 0) {
				return true
			}
		}
		catch (exception) {
			console.error (exception);
		}
		
		return false;
	});
	
	return fresh_treasures;
}

