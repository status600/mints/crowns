
/*
	filter by: 
		trend.name
*/
export const filter_trends_by_name = ({ name_filter, trends }) => {
	if (name_filter === "") {
		return trends
	}
	
	const fresh_trends = trends.filter (trend => {
		try {
			if (Array.isArray (trend.names)) {
				let show = false;
				
				trend.names.forEach (name => {
					console.log ({ name });
					
					if (name.indexOf (name_filter) >= 0) {
						show = true;
					}
				});
				
				return show;
			}
		}
		catch (exception) {
			console.error (exception);
		}
		
		return false;
	});
	
	return fresh_trends;
}