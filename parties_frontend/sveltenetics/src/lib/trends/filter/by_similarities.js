




/*
	filter by: 
		treasure.name
*/
export const filter_trends_by_similar = ({ similarities_filter, trends }) => {
	if (similarities_filter === "") {
		return trends
	}
	
	const fresh_trends = trends.filter (trend => {
		try {
			if (trend.similar && trend.similar.some (item => item.includes(similarities_filter))) {
				return true;
			};
		}
		catch (exception) {
			console.error (exception);
		}
		
		return false;
	});
	
	return fresh_trends;
}

